'use strict';

var gulp =require('gulp'),
    sass =require('gulp-sass');
    browserSync = require('browser-sync').create();

    gulp.task('sass',function(){
    return gulp.src('./CSS/*.scss')
        .pipe(sass().on('error',sass.logError))
        .pipe(gulp.dest('./CSS'));

    })

    gulp.task('sass:watch', function(){
        gulp.watch('./CSS/*.scss',['sass']);
    });

    gulp.task('browser-sync',function(){
        var files=['./*.html' ,'./*.css', './Imagenes/*.{png, jpg, jpeg, gif}' , './JS/*.js']
         browserSync.init(files,{
           server:{
               baseDir:'./'
           } 
        });
    });

    gulp.task('default',['browser-sync'],function(){
        gulp.start('sass:watch');
    });