$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
      interval: 3000  
      });
    $("#Sesion").on("show.bs.modal", function(e){
      console.log("abriendo modal");
      $("#sesionBtn").removeClass("btn-primary");
      $("#sesionBtn").addClass("btn");
      $("#sesionBtn").prop("disabled",true);
    });
    $("#Sesion").on("shown.bs.modal", function(e){
      console.log("se abrio modal");
    });
    $("#Sesion").on("hide.bs.modal" , function(e){
      console.log("cerrando modal");
      $("#sesionBtn").removeClass("btn");
      $("#sesionBtn").addClass("btn-primary");
      $("#sesionBtn").prop("disabled",false);
    });
    $("#Sesion").on("hidden.bs.modal" , function(e){
      console.log("se cerro modal");
    });
  });